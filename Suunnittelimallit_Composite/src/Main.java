
public class Main {

	public static void main(String[] args) {
		
		LaitteenOsa emolevy = new Emolevy();
		LaitteenOsa kotelo = new Kotelo();
		LaitteenOsa keskusmuisti  =new Keskusmuisti();
		LaitteenOsa naytonohjain = new Naytonohjain();
		LaitteenOsa prosessori = new Prosessori();
		LaitteenOsa verkkokortti = new Verkkokortti();
		
		emolevy.lisaaOsa(prosessori);
		emolevy.lisaaOsa(keskusmuisti);
		
		kotelo.lisaaOsa(emolevy);
		kotelo.lisaaOsa(naytonohjain);
		kotelo.lisaaOsa(verkkokortti);
		
		System.out.println("Tietokoneen hinta on: " + kotelo.laskeHinta() + "€");

	}

}
