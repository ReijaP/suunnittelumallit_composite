import java.util.ArrayList;
import java.util.List;

public class Kotelo implements LaitteenOsa {
	
	public double hinta;
	List<LaitteenOsa> osalista = new ArrayList<>();
	
	public Kotelo() {
		hinta = 126;
	}

	@Override
	public double laskeHinta() {
		for(LaitteenOsa s: osalista){ // delegoidaan osille:
			hinta += s.laskeHinta();
		}
		return hinta;
	}

	@Override
	public void lisaaOsa(LaitteenOsa osa) {
		osalista.add(osa);

	}

}
