
public class Verkkokortti implements LaitteenOsa {
	
	public double hinta;
	
	public Verkkokortti() {
		hinta = 20;
	}

	@Override
	public double laskeHinta() {
		return hinta;
	}

	@Override
	public void lisaaOsa(LaitteenOsa osa) {
		throw new RuntimeException
		("Verkkokorttiin ei voi lisätä osaa");

	}

}
