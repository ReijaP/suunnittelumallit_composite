
public class Keskusmuisti implements LaitteenOsa {
	
	public double hinta;
	
	public Keskusmuisti() {
		hinta = 45;
	}

	@Override
	public double laskeHinta() {
		return hinta;
	}

	@Override
	public void lisaaOsa(LaitteenOsa osa) {
		throw new RuntimeException
		("Keskusmuistiin ei voi lisätä osaa");
		}
		

}
