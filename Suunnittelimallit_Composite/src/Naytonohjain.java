
public class Naytonohjain implements LaitteenOsa {
	
	public double hinta;
	
	public Naytonohjain() {
		hinta = 679.70;
	}

	@Override
	public double laskeHinta() {
		return hinta;
	}

	@Override
	public void lisaaOsa(LaitteenOsa osa) {
		throw new RuntimeException
		("Näytönohjaimeen ei voi lisätä osaa");

	}

}
