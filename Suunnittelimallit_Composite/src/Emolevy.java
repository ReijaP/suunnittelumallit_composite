import java.util.ArrayList;
import java.util.List;

public class Emolevy implements LaitteenOsa {
	
	public double hinta;
	List<LaitteenOsa> osalista = new ArrayList<>();
	
	public Emolevy() {
		hinta = 169.90;
	}

	@Override
	public double laskeHinta() {
		for(LaitteenOsa s: osalista){
			hinta += s.laskeHinta();
		}
		return hinta;
	}

	@Override
	public void lisaaOsa(LaitteenOsa osa) {
		osalista.add(osa);
	}

}
