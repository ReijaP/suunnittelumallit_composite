
public class Prosessori implements LaitteenOsa {
	
	public double hinta;
	
	public Prosessori() {
		hinta = 189;
	}

	@Override
	public double laskeHinta() {
		return hinta;
	}

	@Override
	public void lisaaOsa(LaitteenOsa osa) {
		throw new RuntimeException
		("Prosessoriin ei voi lisätä osaa");

	}

}
